p4vasp (0.3.30+dfsg-5) unstable; urgency=medium

  * Fix reproducible build on merged-usr vs non-merged systems,
    thanks Andreas Henriksson (Closes: #915844)
  * Bump Standards-Version to 4.3.0, no changes
  * Update debian/copyright

 -- Graham Inggs <ginggs@debian.org>  Fri, 25 Jan 2019 10:14:23 +0000

p4vasp (0.3.30+dfsg-4) unstable; urgency=medium

  * Drop dependency on libgnomeui-0, thanks Jeremy Bicha! (Closes: #911204)
  * Bump Standards-Version to 4.2.1, no changes

 -- Graham Inggs <ginggs@debian.org>  Fri, 16 Nov 2018 09:32:37 +0000

p4vasp (0.3.30+dfsg-3) unstable; urgency=medium

  * Update Vcs-* URIs for move to salsa.debian.org
  * Depend on python-gobject-2 instead of python-gobject
    transitional package (Closes: #890163)
  * Turn debhelper up to 11
  * Update debian/copyright
  * Fix duplicate word in patch description
  * Bump Standards-Version to 4.1.3, no changes

 -- Graham Inggs <ginggs@debian.org>  Wed, 21 Feb 2018 18:32:11 +0000

p4vasp (0.3.30+dfsg-2) unstable; urgency=medium

  * Do not export PYTHONPATH from /usr/bin/p4v (Closes: #883376)

 -- Graham Inggs <ginggs@debian.org>  Sun, 03 Dec 2017 12:30:19 +0000

p4vasp (0.3.30+dfsg-1) unstable; urgency=medium

  * New upstream release
  * Update debian/watch, debian/rules for new upstream website
  * Drop fix-spelling-errors.patch, refresh remaining
  * Fix path for main window icon
  * Remove trailing whitespace from debian/changelog
  * Update debian/copyright, exclude lib/_cp4vasp.so
  * Bump Standards-Version to 4.1.2, no further changes

 -- Graham Inggs <ginggs@debian.org>  Sat, 02 Dec 2017 14:23:11 +0000

p4vasp (0.3.29+dfsg-4) unstable; urgency=medium

  * Build with -fPIC instead of -fpic to fix FTBFS on
    s390x and sparc64, thanks Adrian Bunk (Closes: #865365)
  * Switch to debhelper 10
  * Drop -pie from DEB_BUILD_MAINT_OPTIONS, no longer needed
  * Use secure URIs for Vcs fields
  * Update d/copyright, https format URL and year
  * Install the icon in the correct location for AppStream
  * Bump Standards-Version to 4.0.0, no further changes

 -- Graham Inggs <ginggs@debian.org>  Wed, 21 Jun 2017 18:15:10 +0200

p4vasp (0.3.29+dfsg-3) unstable; urgency=medium

  * Fix a warning that Applet.glade was missing when
    selecting 'Electronic / View k-points' from the menu
  * Fix '__len__() should return an int' error when
    showing 'Bands' or 'DOS and Bands'
  * Add missing dependency on libgnomeui-0
  * Update build dependency on FLTK to libfltk1.3-dev

 -- Graham Inggs <ginggs@debian.org>  Tue, 17 Nov 2015 14:26:26 +0200

p4vasp (0.3.29+dfsg-2) unstable; urgency=medium

  * Update my email address
  * Update d/copyright, fix dep5-copyright-license-name-not-unique
  * Rename d/patches* to d/patches/*.patch
  * Add dh-python package to Build-Depends
  * Sort applet list to make build reproducible,
    thanks Reiner Herrmann (Closes: #803140)

 -- Graham Inggs <ginggs@debian.org>  Fri, 30 Oct 2015 10:11:41 +0200

p4vasp (0.3.29+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #710249)

 -- Graham Inggs <graham@nerve.org.za>  Sun, 21 Sep 2014 13:52:55 +0200
